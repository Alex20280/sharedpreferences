package layouts.sourceit.com.sharedpreferences;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static layouts.sourceit.com.sharedpreferences.Utils.SP_LOGIN;
import static layouts.sourceit.com.sharedpreferences.Utils.SP_NAME;
import static layouts.sourceit.com.sharedpreferences.Utils.SP_PASS;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.am_login)
    EditText login;
    @BindView(R.id.am_pass)
    EditText pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SharedPreferences sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);
        login.setText(sp.getString(SP_LOGIN, ""));
        pass.setText(sp.getString(SP_LOGIN, ""));

    }


    @OnClick(R.id.am_action_save)
    void onSaveClick() {
        SharedPreferences sp = getSharedPreferences(SP_NAME,MODE_PRIVATE);
        sp.edit()
                .putString(SP_LOGIN, login.getText().toString())
                .putString(SP_PASS, pass.getText().toString())
                .apply();

        sp.edit().clear().apply();

        }
    }


}