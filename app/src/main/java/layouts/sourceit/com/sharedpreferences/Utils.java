package layouts.sourceit.com.sharedpreferences;

import android.os.Environment;

public final class Utils {

    public static final String SP_NAME = "sp_name";
    public static final String SP_LOGIN = "sp_login";
    public static final String SP_PASS = "sp_pass";

    private Utils() {
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}